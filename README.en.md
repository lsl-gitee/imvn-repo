# imvn-repo

#### Introduce
Personal Maven private server, mainly is the personal development of some tool JAR package.

#### Usage
Modification maven configuration file `settings.xml`
- Find the `</profiles>` tab and add the child items as follows：
```xml
<profile>
    <id>imvn-repo</id>
    <repositories>
        <repository>
            <id>imvn-repo</id>
            <url>https://gitee.com/lsl-gitee/imvn-repo/raw/master/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
</profile>
```
- Find the `</activeProfiles>` tab and add the child items as follows：
```xml
<activeProfile>imvn-repo</activeProfile>
```