# imvn-repo

#### 介绍
个人maven私服，主要是个人开发的一些工具jar包。

#### 使用私服
修改maven配置文件settings.xml
- 找到`</profiles>`标签并添加子项，内容如下：
```xml
<profile>
    <id>imvn-repo</id>
    <repositories>
        <repository>
            <id>imvn-repo</id>
            <url>https://gitee.com/lsl-gitee/imvn-repo/raw/master/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
</profile>
```
- 找到`</activeProfiles>`标签并添加子项，内容如下：
```xml
<activeProfile>imvn-repo</activeProfile>
```